﻿Type=Activity
Version=5.02
ModulesStructureVersion=1
B4A=true
@EndOfDesignText@
#Region  Activity Attributes 
	#FullScreen: False
	#IncludeTitle: True
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.
	Public ID As Int
	Public SUBJECT_TITLE As String
	
End Sub

Sub Globals
	'These global variables will be redeclared each time the activity is created.
	'These variables can only be accessed from this module.
	Private SUBJECT_ID As Int
	Private dtAssigned As Long
	Private dtDue As Long
	Private dtAssignedDate As DateDialog
	Private dtDueDate As DateDialog

	Private spinSubject As Spinner
	Private txtNote As EditText
	Private btnSave As Button
	Private btnDelete As Button
	Private btnCancel As Button
	Private btnAssignDate As Button
	Private btnDueDate As Button
	
End Sub

Sub Activity_Create(FirstTime As Boolean)
	DateTime.DateFormat = "dd/MM/yyyy"
	'Do not forget to load the layout file created with the visual designer. For example:
	Activity.LoadLayout("Job")
	FillSpinners
	
	If ID = 0 Then
		dtAssigned = DateTime.Now
		btnAssignDate.Text = DateTime.Date(dtAssigned)
		dtDue = DateTime.Add(DateTime.Now, 0, 0, 1)
		btnDueDate.Text = DateTime.Date(dtDue)
	Else
		ShowJob
		Dim i As Int
		For i = 0 To spinSubject.Size -1
			If SUBJECT_TITLE = spinSubject.GetItem(i) Then
				spinSubject.SelectedIndex = i
				Exit
			End If
		Next
		
	End If
End Sub

Sub Activity_Resume

End Sub

Sub Activity_Pause (UserClosed As Boolean)

End Sub

Sub SaveJob
	GetSubjectIdFromTitle(spinSubject.SelectedItem)
	If ID = 0 Then
		Main.SQL1.ExecNonQuery("INSERT INTO Jobs (SUBJECT_ID, NOTE, ASSIGNED_AT, DUE_AT, STATUS) VALUES ('" & SUBJECT_ID & "','" & txtNote.Text & "','" & btnAssignDate.Text & "','" & btnDueDate.Text & "',0)")
		ToastMessageShow("Inserted job.", False)
	Else
		Main.SQL1.ExecNonQuery("UPDATE Jobs SET SUBJECT_ID='" & SUBJECT_ID & "', NOTE='" & txtNote.Text & "', ASSIGNED_AT='" & btnAssignDate.Text & "', DUE_AT='" & btnDueDate.Text & "' WHERE ID=" & ID)
		ToastMessageShow("Updated job.", False)
	End If
End Sub

Sub ShowJob
	Dim Cursor1 As Cursor
	Cursor1 = Main.SQL1.ExecQuery("SELECT * FROM Jobs WHERE ID=" & ID)
	If Cursor1.RowCount > 0 Then 'check if entries exist
		Cursor1.Position = 0
		SUBJECT_ID = Cursor1.GetInt("SUBJECT_ID")
		txtNote.Text = Cursor1.GetString("NOTE")
		btnAssignDate.Text = Cursor1.GetString("ASSIGNED_AT")
		dtAssigned = DateTime.DateParse(btnAssignDate.Text)
		btnDueDate.Text = Cursor1.GetString("DUE_AT")
		dtDue = DateTime.DateParse(btnDueDate.Text)
	End If
	Cursor1.Close 'close the cursor, we don't need it anymore
End Sub

Sub DeleteJob
	Main.SQL1.ExecNonQuery("DELETE FROM Jobs WHERE ID=" & ID)
	ToastMessageShow("Deleted job.", False)
End Sub

Sub FillSpinners
	Dim Query As String
	Query = "SELECT TITLE, ID FROM Subjects"
	spinSubject.Clear
	DBUtils.ExecuteSpinner(Main.SQL1, Query, Null, 0, spinSubject)
	
End Sub

Sub GetSubjectIdFromTitle(TITLE As String)
	SUBJECT_ID = Main.SQL1.ExecQuerySingleResult("SELECT ID FROM Subjects WHERE TITLE='" & TITLE & "'")
End Sub

Sub spinSubject_ItemClick (Position As Int, Value As Object)
	GetSubjectIdFromTitle(Value)
End Sub

Sub btnSave_Click
	SaveJob
End Sub

Sub btnDelete_Click
	DeleteJob
End Sub

Sub btnCancel_Click
	Activity.Finish
End Sub

Sub btnAssignDate_Click

	dtAssignedDate.Year = DateTime.GetYear(dtAssigned)
	dtAssignedDate.Month = DateTime.GetMonth(dtAssigned)
	dtAssignedDate.DayOfMonth = DateTime.GetDayOfMonth(dtAssigned)
	
	Dim ret As Int
    ret = dtAssignedDate.Show("Message", "Title", "OK", "Cancel", "", Null)
	If ret = DialogResponse.POSITIVE Then
		dtAssigned = dtAssignedDate.DateTicks
		btnAssignDate.Text = DateTime.Date(dtAssigned)
	End If
End Sub

Sub btnDueDate_Click
	
	dtDueDate.Year = DateTime.GetYear(dtDue)
	dtDueDate.Month = DateTime.GetMonth(dtDue)
	dtDueDate.DayOfMonth = DateTime.GetDayOfMonth(dtDue)
	
	Dim ret As Int
    ret = dtDueDate.Show("Message", "Title", "OK", "Cancel", "", Null)
	If ret = DialogResponse.POSITIVE Then
		dtDue = dtDueDate.DateTicks
		btnDueDate.Text = DateTime.Date(dtDue)
	End If
End Sub
