﻿Type=Activity
Version=5.02
ModulesStructureVersion=1
B4A=true
@EndOfDesignText@
#Region  Activity Attributes 
	#FullScreen: False
	#IncludeTitle: True
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.
'	Private SQL1 As SQL

End Sub

Sub Globals
	'These global variables will be redeclared each time the activity is created.
	'These variables can only be accessed from this module.

	Private btnAdd As Button
	Private wvwJobs As WebView
	Private listJobs As List
End Sub

Sub Activity_Create(FirstTime As Boolean)
	'Do not forget to load the layout file created with the visual designer. For example:
	Activity.LoadLayout("JobList")
End Sub

Sub Activity_Resume
'	If Main.SQL1.IsInitialized = False Then
'		Main.SQL1.Initialize(File.DirInternal, "Homework.db", True)
'	End If
	ShowTableInWebView

End Sub

Sub Activity_Pause (UserClosed As Boolean)
'	If UserClosed Then
'		Main.SQL1.Close
'	End If

End Sub

Sub btnAdd_Click
	Job.ID = 0
	StartActivity(Job)
End Sub

Sub ShowTableInWebView
	Dim Query As String
	Query = "SELECT Jobs.ID, Subjects.TITLE, Jobs.NOTE, Jobs.ASSIGNED_AT, Jobs.DUE_AT, Jobs.STATUS FROM Jobs INNER JOIN Subjects ON Jobs.SUBJECT_ID = Subjects.ID"
	wvwJobs.LoadHtml(DBUtils.ExecuteHtml(Main.SQL1, Query, Null, 0, True))
	listJobs = DBUtils.ExecuteMemoryTable(Main.SQL1, Query, Null, 0)
End Sub

Sub wvwJobs_OverrideUrl (Url As String) As Boolean
	Log(Url)
	'parse the row and column numbers from the URL
	Dim values() As String
	values = Regex.Split("[.]", Url.SubString(7))
	Dim col, row As Int
	col = values(0)
	row = values(1)
	EditJob(row)
	ToastMessageShow("User pressed on column: " & col & " and row: " & row, False)
	Return True 'Don't try to navigate to this URL
End Sub

Sub EditJob(row As Int)
	Dim JobData() As String = listJobs.Get(row)
	Job.ID = JobData(0)
	Job.SUBJECT_TITLE = JobData(1)
	StartActivity(Job)
End Sub