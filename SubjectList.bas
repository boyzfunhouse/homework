﻿Type=Activity
Version=5.02
ModulesStructureVersion=1
B4A=true
@EndOfDesignText@
#Region  Activity Attributes 
	#FullScreen: False
	#IncludeTitle: True
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.
'	Private SQL1 As SQL
	
	Dim RowNumber = 0 As Int ' number of rows
End Sub

Sub Globals
	'These global variables will be redeclared each time the activity is created.
	'These variables can only be accessed from this module.

	Private btnAdd As Button
	Private lstSubject As ListView
End Sub

Sub Activity_Create(FirstTime As Boolean)
	'Do not forget to load the layout file created with the visual designer. For example:
	Activity.LoadLayout("SubjectList")
	
End Sub

Sub Activity_Resume
'	If SQL1.IsInitialized = False Then
'		SQL1.Initialize(File.DirInternal, "Homework.db", True)
'	End If
	ShowSubjects
End Sub

Sub Activity_Pause (UserClosed As Boolean)
'	If UserClosed Then
'		SQL1.Close
'	End If
End Sub

Sub ShowSubjects
	Dim Row As Int
	Dim Cursor1 As Cursor
	Cursor1 = Main.SQL1.ExecQuery("SELECT * FROM Subjects")
	If Cursor1.RowCount > 0 Then 'check if entries exist
		lstSubject.Clear
		RowNumber = Cursor1.RowCount '5
		For Row = 0 To RowNumber - 1 '0 -> 4
			Cursor1.Position = Row 'set the Cursor to each row
			lstSubject.AddSingleLine2(Cursor1.GetString("TITLE"), Cursor1.GetInt("ID"))
		Next
	End If
	Cursor1.Close 'close the cursor, we don't need it anymore
End Sub

Sub btnAdd_Click
	Subject.ID = 0
	StartActivity(Subject)
End Sub

Sub lstSubject_ItemLongClick (Position As Int, Value As Object)
	Subject.ID = Value
	StartActivity(Subject)
End Sub