﻿Type=Activity
Version=5.02
ModulesStructureVersion=1
B4A=true
@EndOfDesignText@
#Region  Activity Attributes 
	#FullScreen: False
	#IncludeTitle: True
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.
'	Private SQL1 As SQL
	Public ID As Int

End Sub

Sub Globals
	'These global variables will be redeclared each time the activity is created.
	'These variables can only be accessed from this module.

	Private txtCode As EditText
	Private txtSubject As EditText
	Private btnSave As Button
	Private btnDelete As Button
	Private btnCancel As Button
End Sub

Sub Activity_Create(FirstTime As Boolean)
	'Do not forget to load the layout file created with the visual designer. For example:
	Activity.LoadLayout("Subject")

End Sub

Sub Activity_Resume
'	If SQL1.IsInitialized = False Then
'		SQL1.Initialize(File.DirInternal, "Homework.db", True)
'	End If

	If ID <> 0 Then
		ShowSubject
	End If
	
End Sub

Sub Activity_Pause (UserClosed As Boolean)
'	If UserClosed Then
'		SQL1.Close
'	End If
End Sub

Sub SaveSubject
	If ID = 0 Then
		Main.SQL1.ExecNonQuery("INSERT INTO Subjects (CODE, TITLE) VALUES ('" & txtCode.Text & "','" & txtSubject.Text & "')")
		ToastMessageShow("Inserted subject.", False)
	Else
		Main.SQL1.ExecNonQuery("UPDATE Subjects SET CODE='" & txtCode.Text & "', TITLE='" & txtSubject.Text & "' WHERE ID=" & ID)
		ToastMessageShow("Updated subject.", False)
	End If
End Sub

Sub ShowSubject
	Dim Cursor1 As Cursor
	Cursor1 = Main.SQL1.ExecQuery("SELECT * FROM Subjects WHERE ID=" & ID)
	If Cursor1.RowCount > 0 Then 'check if entries exist
		Cursor1.Position = 0
		txtCode.Text = Cursor1.GetString("CODE")
		txtSubject.Text = Cursor1.GetString("TITLE")
	End If
	Cursor1.Close 'close the cursor, we don't need it anymore
End Sub

Sub DeleteSubject
	Main.SQL1.ExecNonQuery("DELETE FROM Subjects WHERE ID=" & ID)
	ToastMessageShow("Deleted subject.", False)
End Sub

Sub btnSave_Click
	SaveSubject
	Activity.Finish
End Sub

Sub btnDelete_Click
	DeleteSubject
	Activity.Finish
End Sub

Sub btnCancel_Click
	Activity.Finish
End Sub